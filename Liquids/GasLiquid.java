package liquids

public class GasLiquid extends AGasLiquid {
    private String name;
    private int sugar;
    private int color;
    
    public GasLiquid(String name, int sugar, int color) {
	this.name = name;
	this.sugar = sugar;
	this.color = color;
    }

    public void drink() {
	System.out.println("I drink " + this.name);
    }

    
}
