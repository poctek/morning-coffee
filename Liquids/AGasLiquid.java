package liquids;

public abstract class AGasLiquid  implements ILiquid {
    protected int sugar;
    protected int color;

    public abstract void drink();
    public int getSugar() {
	return this.sugar;
    }
    public int getColor() {
	return this.color;
    }
}
