package liquids;

public interface ICaffeineLiquid {
    public int getCaffeine();
}
