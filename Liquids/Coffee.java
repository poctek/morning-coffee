package liquids;

public class Coffee implements ICaffeineLiquid, ILiquid {
    private int caffeine;

    public Coffee(int caffeine) {
	this.caffeine = caffeine;
    }

    public int getCaffeine() {
	return this.caffeine;
    }

    public void drink() {
	System.out.println("I drink coffee");
    }
}
