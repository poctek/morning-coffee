import liquids.*;
import exceptions.*;

public abstract class AConsumer {
    private int sugarLevel;
    private int caffeineLevel;

    public void drinkGas(AGasLiquid liquid) {
	liquid.drink();
	if (this.sugarLevel + liquid.getSugar() > getSugarLD50()) {
	    throw new SugarLD50;
	}
	this.sugarLevel += liquid.getSugar();
    }

    public void drinkCaf(ICaffeine liquid) {
	liquid.drink();
	if (this.caffeineLevel + liquid.getCaffeine() > getCaffeineLD50) {
	    throw new CaffeineLD50;
	}
    }

    public int getSugarLevel() {
	return this.sugarLevel;
    }

    public int getCaffeineLevel() {
	return this.caffeineLevel();
    }

    public void setSugarLevel(int level) {
	this.sugarLevel = level;
    }

    public void setCaffeineLevel(int level) {
	this.caffeineLevel = level;
    }

    public abstract int getSugarLD50();
    public abstract int   getCaffeineLD50();
}
