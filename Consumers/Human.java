import liquids.*;
import exceptions.*;

public class Human extends AConsumer {
    private int sugarLD50;
    private int caffeineLD50;

    public void Human(int sugar, int caffeine) {
	this.sugarLD50 = sugar;
	this.caffeineLD50 = caffeine;
    }

    public int getSugarLD50() {
	return this.sugarLD50;
    }

    public int getCaffeineLD50() {
	return this.caffeineLD50;
    }
}
