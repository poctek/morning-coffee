package companies;
import liquids.*;
import exceptions.*;

public class PepsiCo implements ICompany {
    ILiquid produce(int color) {
	if (color != 3) {
	    throw new NoSuchLiquid();
	}
	return new PepsiCola(3, color);
    }
}
