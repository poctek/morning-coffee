package companies;
import liquids.*;
import exceptions.*;

public class CocaCo implements ICompany {
    ILiquid produce(int color) {
	if (color != 5) {
	    throw new NoSuchLiquid();
	}
	return new CocaCola(5, color);
    }
}
