package companies;
import exceptions.*;
import liquds.*;

public interface ICompany {
    ILiquid produce(int color) throws NoSuchLiquid;
}
